package view;
import java.io.*;
import java.util.Scanner;

import model.Aluno;
import model.Disciplina;

import control.ControleAluno;
import control.ControleDisciplina;

public class CadastroAlunoDisciplina{

	public static void main(String [] args) throws IOException{
	//burocracia
	InputStream entradaSistema = System.in;
	InputStreamReader leitor = new InputStreamReader(entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;

	//instaciando
	ControleAluno umControleAluno = new ControleAluno();
	ControleDisciplina umControleDisciplina = new ControleDisciplina();
	Aluno umAluno = new Aluno();
	Disciplina umaDisciplina = new Disciplina();

	//declarando variável de loop
    	boolean continuar = true;

	//variavel menu
	int opcao;
	Scanner entrada = new Scanner(System.in);


	while(continuar){ 

	//menu

		System.out.println("\tMenu");
		System.out.println("1 - Adicionar Disciplina");
		System.out.println("2 - Adicionar Aluno");
		System.out.println("3 - Adicionar Aluno na Disciplina");
		System.out.println("4 - Remover Disciplina");
		System.out.println("5 - Remover Aluno");
		System.out.println("6 - Remover Aluno da Disciplina");
		System.out.println("7 - Pesquisar Disciplina");
		System.out.println("8 - Pesquisar Aluno");
		System.out.println("9 - Pesquisar Aluno na Disciplina");
		System.out.println("0 - Sair");

		System.out.println("Escolha uma opção");
		opcao = entrada.nextInt();
		
		switch(opcao)
		{
				case 1:
						System.out.println("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						umaDisciplina.setDisciplina(entradaTeclado);

						System.out.println("Digite o nome do professor: ");
						entradaTeclado = leitorEntrada.readLine();
						umaDisciplina.setProfessor(entradaTeclado);

						String mensagem = umControleDisciplina.adicionar(umaDisciplina);
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem);
						System.out.println("=)\n");
						break;

				case 2:
						System.out.println("Digite o nome do aluno: ");
						entradaTeclado = leitorEntrada.readLine();
						umAluno.setNome(entradaTeclado);

						System.out.println("Digite a matrícula do aluno: ");
						entradaTeclado = leitorEntrada.readLine();
						umAluno.setMatricula(entradaTeclado);

						String mensagem2 = umControleAluno.adicionar(umAluno);
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem2);
						System.out.println("=)\n");
						break;
				
				case 3:
						System.out.println("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						Disciplina DisciplinaPesquisada = umControleDisciplina.pesquisar(entradaTeclado);
						System.out.println("Digite o nome do aluno que deseja adicionar: ");
						entradaTeclado = leitorEntrada.readLine();
						Aluno alunos = umControleAluno.pesquisar(entradaTeclado);
						
						
						String mensagem3 = DisciplinaPesquisada.adicionarAluno(alunos);
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem3);
						System.out.println("=)\n");
						break;

				case 4:
						System.out.println("Digite o nome da disciplina que deseja remover: ");
						entradaTeclado = leitorEntrada.readLine();
						Disciplina DisciplinaPesquisar = umControleDisciplina.pesquisar(entradaTeclado);

						
						String mensagem4 = umControleDisciplina.remover(DisciplinaPesquisar);
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem4);
						System.out.println("=)\n");
						break;

				case 5:
						System.out.println("Digite o nome do Aluno que deseja remover: ");
						entradaTeclado = leitorEntrada.readLine();
						Aluno AlunoPesquisar = umControleAluno.pesquisar(entradaTeclado);
						
						
						String mensagem5 = umControleAluno.remover(AlunoPesquisar);
						
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem5);
						System.out.println("=)\n");
						break;

				case 6:
						System.out.println("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						Disciplina DisciplinaPesquisada2 = umControleDisciplina.pesquisar(entradaTeclado);
						System.out.println("Digite o nome do aluno que deseja remover: ");
						entradaTeclado = leitorEntrada.readLine();
						Aluno AlunoRemover = DisciplinaPesquisada2.pesquisarAluno(entradaTeclado);

						
						String mensagem6 = DisciplinaPesquisada2.removerAluno(AlunoRemover);
						
						//conferindo saída
						System.out.println("=================================");
	    					System.out.println(mensagem6);
						System.out.println("=)\n");
						break;

				case 7:
						System.out.println("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						Disciplina mensagem7 = umControleDisciplina.pesquisar(entradaTeclado);
						
						//conferindo saída
						System.out.println("Disciplina: " + mensagem7.getDisciplina() + " Professor: " + mensagem7.getProfessor() + "."); 
						break;

				case 8:
						System.out.println("Digite o nome do aluno: ");
						entradaTeclado = leitorEntrada.readLine();
						Aluno mensagem8 = umControleAluno.pesquisar(entradaTeclado);
						
						//conferindo saída
						System.out.println("Nome: " + mensagem8.getNome() + " Matrícula: " + mensagem8.getMatricula() + "."); 
						break;

				case 9:
						System.out.println("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						Disciplina DisciplinaPesquisada3 = umControleDisciplina.pesquisar(entradaTeclado);
						System.out.println("\nDigite o nome do aluno que deseja pesquisar: ");
						entradaTeclado = leitorEntrada.readLine();

						Aluno mensagem9 = DisciplinaPesquisada3.pesquisarAluno(entradaTeclado);
						
						//conferindo saída
						System.out.println("=================================");
					        System.out.println("Alunos regularmente matriculado na discipina!!!");
						System.out.println("Nome: " + mensagem9.getNome() + " Matrícula: " + mensagem9.getMatricula() + "."); 
						break;

				default:

			    			continuar=false;
						System.out.println("Obrigado!!!");

				





		}

	
	}
	
	}

}
