package control;
import java.util.ArrayList;

import model.Aluno;

public class ControleAluno{
	//atributos

	private ArrayList<Aluno> listaAlunos;

	//construtor
	
	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	}

	//métodos

	public String adicionar(Aluno umAluno){
		String mensagem = "Aluno matriculado"; 
		listaAlunos.add(umAluno);
		return mensagem;
	}

	public String remover(Aluno umaAluna){
		String mensagem = "Aluno jubilado";
		listaAlunos.remove(umaAluna);
		return mensagem;
	}

	public Aluno pesquisar(String umNome){
		for(Aluno alunoPesquisado: listaAlunos){
			if(alunoPesquisado.getNome().equalsIgnoreCase(umNome)){
				return alunoPesquisado;
			}
		}
	return null;
	}
}

