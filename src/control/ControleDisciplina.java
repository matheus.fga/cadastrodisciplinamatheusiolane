package control;
import java.util.ArrayList;

import model.Disciplina;

public class ControleDisciplina{

	//atributos

	private ArrayList<Disciplina> listaDisciplinas;

	//construtor
	
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}

	//métodos

	public String adicionar(Disciplina umaDisciplina){
		String mensagem = "Disciplina adicionada"; 
		listaDisciplinas.add(umaDisciplina);
		return mensagem;
	}

	public String remover(Disciplina umDisciplina){
		String mensagem = "Disciplina removida"; 
		listaDisciplinas.remove(umDisciplina);
		return mensagem;
	}


	public Disciplina pesquisar(String umNomeDisciplina){
		for(Disciplina disciplinaPesquisada: listaDisciplinas){
			if(disciplinaPesquisada.getDisciplina().equalsIgnoreCase(umNomeDisciplina)){
				return disciplinaPesquisada;
			}
		}
		return null;
		}
	

}
