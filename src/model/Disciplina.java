package model;
import java.util.ArrayList;

public class Disciplina{

	private String nome;
	private String professor;
	private ArrayList<Aluno> listaAluno;


//construtor

		public Disciplina(){
			listaAluno = new ArrayList<Aluno>();
		}

//Métodos

	public void setDisciplina(String umaDisciplina){
		nome = umaDisciplina;	
	}
	
	public String getDisciplina(){
		return nome;
	}

	public void setProfessor(String umProfessor){
		professor = umProfessor;
	}

	public String getProfessor(){
		return professor;
	}

	public String adicionarAluno(Aluno umAluno){
		String mensagem = "Aluno matriculado na disciplina com sucesso!!!";
		listaAluno.add(umAluno);
		return mensagem;
	}

	public String removerAluno(Aluno umaAluna){
		String mensagem = "Aluno removido da disciplina com sucesso!!!";
		listaAluno.remove(umaAluna);
		return mensagem;
	}

	public Aluno pesquisarAluno(String umNome){
		for(Aluno alunoPesquisado: listaAluno){
			if(alunoPesquisado.getNome().equalsIgnoreCase(umNome)){
				return alunoPesquisado;
			}
		}
		return null;
	}

}
