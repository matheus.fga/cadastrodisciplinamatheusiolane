package model;

public class Aluno{

	//atributos
	private String nome;
	private String matricula;

	//construtor
	public Aluno(){
	}

	//métodos
	public void setNome(String umNome){
		nome = umNome;
	}

	public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
	}

	public String getNome(){
		return nome;
	}

	public String getMatricula(){
		return matricula;
	}
	
}
