package test;

import static org.junit.Assert.*;

import model.Aluno;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class TestModel {
	
	private Aluno umAluno;
	private Disciplina umaDisciplina;
	
	@Before
	public void setUp()throws Exception{
		umAluno = new Aluno();
		umaDisciplina = new Disciplina();
	}

	@Test
	public void testNomeAluno() {
		umAluno.setNome("Matheus");
		assertEquals("Matheus", umAluno.getNome());
		
	}
	
	@Test
	public void testMatriculaAluno() {
		umAluno.setMatricula("xx/xxxx");
		assertEquals("xx/xxxx", umAluno.getMatricula());
		
	}
	
	@Test
	public void testNomeDisciplina() {
		umaDisciplina.setDisciplina("OO");
		assertEquals("OO", umaDisciplina.getDisciplina());
		
	}
	
	@Test
	public void testProfessorDisciplina() {
		umaDisciplina.setProfessor("Paulo");
		assertEquals("Paulo", umaDisciplina.getProfessor());
		
	}
	
	
}
