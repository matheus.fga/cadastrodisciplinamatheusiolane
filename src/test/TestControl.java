package test;

import static org.junit.Assert.*;

import model.Aluno;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

import control.ControleAluno;
import control.ControleDisciplina;

public class TestControl {
	
private ControleAluno umAlunoControle;
private Aluno umAluno;
private ControleDisciplina umControleDisciplina;
private Disciplina umaDisciplina;




	@Before
	public void setUp()throws Exception{
		umAlunoControle = new ControleAluno();
		umAluno = new Aluno();
		umControleDisciplina = new ControleDisciplina();
		umaDisciplina = new Disciplina();
	}

	@Test
	public void testAdicionar() {		
		assertEquals("Aluno matriculado", umAlunoControle.adicionar(umAluno));
		
	}
	
	@Test
	public void testRemover() {		
		assertEquals("Aluno jubilado", umAlunoControle.remover(umAluno));
		
	}
	
	@Test
	public void testPesquisar() {
		String umNome = "Matheus";
		umAluno.setNome(umNome);
		umAlunoControle.adicionar(umAluno);
		
		assertEquals(umAluno, umAlunoControle.pesquisar(umNome));
		
	}

	@Test
	public void testAdicionarDisciplina() {		
		assertEquals("Disciplina adicionada", umControleDisciplina.adicionar(umaDisciplina));
		
	}
	
	@Test
	public void testRemoverDisciplina() {		
		assertEquals("Disciplina removida", umControleDisciplina.remover(umaDisciplina));
		
	}
	
	@Test
	public void testPesquisarDisciplina() {
		String umNomeDiciplina = "Fisica";
		umaDisciplina.setDisciplina(umNomeDiciplina);
		umControleDisciplina.adicionar(umaDisciplina);
		
		assertEquals(umaDisciplina, umControleDisciplina.pesquisar(umNomeDiciplina));
		
	}
	
	@Test
	public void testAdicionarAlunoNaDisciplina() {		
		assertEquals("Aluno matriculado na disciplina com sucesso!!!", umaDisciplina.adicionarAluno(umAluno));
		
	}
	
	@Test
	public void testRemoverAlunoDaDisciplina() {		
		assertEquals("Aluno removido da disciplina com sucesso!!!", umaDisciplina.removerAluno(umAluno));
		
	}
	
	@Test
	public void testPesquisarAlunoNaDisciplina() {
		String umNomeAluno = "Iolane";
		umAluno.setNome(umNomeAluno);
		umaDisciplina.adicionarAluno(umAluno);
		
		assertEquals(umAluno, umaDisciplina.pesquisarAluno(umNomeAluno));
		
	}


}
